# Automation priority: null
# Test case importance: High
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_245_SETUP}	Get Variable Value	${TEST 245 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_245_SETUP is not None	${__TEST_245_SETUP}

Test Teardown
    ${__TEST_245_TEARDOWN}	Get Variable Value	${TEST 245 TEARDOWN}
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	Run Keyword If	$__TEST_245_TEARDOWN is not None	${__TEST_245_TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}

*** Test Cases ***
Cannot log with incorrect password
	[Setup]	Test Setup

	Given I am on the AccountCreation page
	When I fill AccountCreation fields with gender "F" firstName "Alice" lastName "Noel" password "police" email "alice@noel.com" birthDate "01/01/1970" acceptPartnerOffers "yes" acceptPrivacyPolicy "yes" acceptNewsletter "yes" acceptGpdr "yes" and submit
	And I sign out
	And I sign in with email "alice@noel.com" and password "poluce"
	Then The error message "Échec d'authentification" is displayed

	[Teardown]	Test Teardown